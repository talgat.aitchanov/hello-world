package kz.talgat.reverse;

public class ReverseArray {

    private static final char WORDS_DELIMITER = ' ';

    public static void main(String[] args) {
//        char[] anyArray = new char[]{'l', ' ', 'l', 'o', 'v', 'e', ' ', 't', 'a', 'x', 'i'};
        char[] anyArray = new char[]{'n', 'u', 'm', ' ', 'l', 'o', 'v', 'e', ' ', 'a', 'b', 'c', ' ', 't', 'a', 'x', 'i'};
        reverseArrayElements(anyArray);
    }

    private static void reverseArrayElements(char[] charArray) {
        System.out.println("initial array:");
        System.out.println(charArray);

        int wordDelimitersIdx = 0;
        for (int mainCursorIdx = 0; mainCursorIdx < charArray.length; mainCursorIdx++) {

            if (mainCursorIdx == charArray.length - 1) {
                reverse(charArray, mainCursorIdx, wordDelimitersIdx);
                break;
            }

            if (charArray[mainCursorIdx + 1] == WORDS_DELIMITER) {
                if (wordDelimitersIdx == 0) {
                    reverseForTheBeginning(charArray, mainCursorIdx, wordDelimitersIdx);
                } else {
                    reverse(charArray, mainCursorIdx, wordDelimitersIdx);
                }
                wordDelimitersIdx = mainCursorIdx + 1;
            }

        }

        System.out.println("reversed array:");
        System.out.println(charArray);
    }

    private static void reverse(char[] charArray, int mainCursorIdx, int wordDelimitersIdx) {
        for (int forwardIdx = wordDelimitersIdx + 1; forwardIdx <= mainCursorIdx - (mainCursorIdx - wordDelimitersIdx + 1) / 2; forwardIdx++) {
            char tmp = charArray[forwardIdx];
            charArray[forwardIdx] = charArray[mainCursorIdx - forwardIdx + wordDelimitersIdx + 1];
            charArray[mainCursorIdx - forwardIdx + wordDelimitersIdx + 1] = tmp;
        }
    }

    private static void reverseForTheBeginning(char[] charArray, int mainCursorIdx, int wordDelimitersIdx) {
        for (int forwardIdx = wordDelimitersIdx; forwardIdx <= mainCursorIdx / 2; forwardIdx++) {
            char t = charArray[forwardIdx];
            charArray[forwardIdx] = charArray[mainCursorIdx - forwardIdx];
            charArray[mainCursorIdx - forwardIdx] = t;
        }
    }

}
