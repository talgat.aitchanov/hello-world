package kz.talgat.reverse;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class Main {

    private static final char WORDS_DELIMITER = ' ';

    public static void main(String[] args) {
        char[] anyArray = new char[]{'l', ' ', 'l', 'o', 'v', 'e', ' ', 't', 'a', 'x', 'i'};
        reverseArrayElements(anyArray);
    }

    private static void reverseArrayElements(char[] charArray) {
        System.out.println("initial array:");
        System.out.println(charArray);

        List<Stack<Character>> listOfStack = retrieveListOfStacks(charArray);
        charArray = getReversedArray(listOfStack, charArray);

        System.out.println("reversed array:");
        System.out.println(charArray);
    }

    private static List<Stack<Character>> retrieveListOfStacks(char[] charArray) {
        List<Stack<Character>> listOfStack = new ArrayList<>();
        Stack<Character> characterStack = new Stack<>();

        for (int i = 0; i < charArray.length; i++) {
            if (charArray[i] == WORDS_DELIMITER) {
                listOfStack.add(characterStack);
                characterStack = new Stack<>();
                continue;
            }

            characterStack.push(charArray[i]);

            if (i == charArray.length - 1) {
                listOfStack.add(characterStack);
            }

        }
        return listOfStack;
    }

    private static char[] getReversedArray(List<Stack<Character>> listOfStack, char[] charArray) {
        char[] newCharArray = charArray.clone();

        int currentIndex = 0;
        for (Stack<Character> stack : listOfStack) {
            while (!stack.isEmpty()) {
                newCharArray[currentIndex] = stack.pop();
                currentIndex++;
            }

            if (currentIndex < newCharArray.length - 1) {
                currentIndex++;
//                newCharArray[currentIndex] = WORDS_DELIMITER;
            }
        }
        return newCharArray;
    }

}
